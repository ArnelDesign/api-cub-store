<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class WarehousesMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('warehouses', function(Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->longText('address');
            $table->string('latitude');
            $table->string('length');
            $table->unsignedInteger('city_id');
            $table->unsignedInteger('status_id');
            $table->integer('principal')->default(0);
            $table->timestamps();

            $table->foreign('city_id')->references('id')->on('cities')->onUpdate('no action')->onDelete('no action');
            $table->foreign('status_id')->references('id')->on('statuses')->onUpdate('no action')->onDelete('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('warehouses');
    }
}
