<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CitiesMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cities', function(Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->string('acronym');
            $table->double('weight_per_household')->default('100');
            $table->integer('minimum_number_orders')->default(1);
            $table->integer('maximum_number_orders')->default(100);
            $table->string('latitude');
            $table->string('length');
            $table->integer('cashback_payment_percentage')->default(50);
            $table->unsignedInteger('departament_id');
            $table->unsignedInteger('status_id');
            $table->timestamps();

            $table->foreign('departament_id')->references('id')->on('departaments')->onUpdate('no action')->onDelete('no action');
            $table->foreign('status_id')->references('id')->on('statuses')->onUpdate('no action')->onDelete('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cities');
    }
}
