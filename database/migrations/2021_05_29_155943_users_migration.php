<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UsersMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('users', function(Blueprint $table){
            $table->increments('id');
            $table->string('uid');
            $table->string('names');
            $table->string('last_names');
            $table->longText('email');
            $table->longText('avatar');
            $table->longText('contraseña');
            $table->longText('password');
            $table->unsignedInteger('document_type_id');
            $table->string('document');
            $table->unsignedInteger('city_id');
            $table->unsignedInteger('account_type_id');
            $table->unsignedInteger('status_id');
            $table->integer('quantity_orders')->default(0);
            $table->dateTime('first_order_date')->nullable();
            $table->dateTime('last_order_date')->nullable();
            $table->timestamps();

            $table->foreign('document_type_id')->references('id')->on('document_types')->onUpdate('no action')->onDelete('no action');
            $table->foreign('city_id')->references('id')->on('cities')->onUpdate('no action')->onDelete('no action');
            $table->foreign('account_type_id')->references('id')->on('account_types')->onUpdate('no action')->onDelete('no action');
            $table->foreign('status_id')->references('id')->on('statuses')->onUpdate('no action')->onDelete('no action');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
