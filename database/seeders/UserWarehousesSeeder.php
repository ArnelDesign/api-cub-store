<?php

namespace Database\Seeders;

use App\Models\UserWarehouse;
use Illuminate\Database\Seeder;

class UserWarehousesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserWarehouse::create([
            'user_id' => 1,
            'warehouse_id' => 1,
            'status_id' => 1,
            'verification_code' => 0
        ]);
    }
}
