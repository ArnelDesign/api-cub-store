<?php

namespace Database\Seeders;

use App\Models\Country;
use Illuminate\Database\Seeder;

class CountriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Country::create([
            'name' => 'Colombia',
            'vat_value' => 19,
            'code_country' => 57,
            'status_id' => 1
        ]);
    }
}
