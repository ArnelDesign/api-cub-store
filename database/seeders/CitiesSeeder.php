<?php

namespace Database\Seeders;

use App\Models\City;
use Illuminate\Database\Seeder;

class CitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        City::create([
            'name' => 'Bogotá D.C.',
            'acronym' => 'BOG',
            'weight_per_household' => 50,
            'minimum_number_orders' => 1,
            'maximum_number_orders' => 4,
            'latitude' => '4.6482422',
            'length' => '-74.388021',
            'cashback_payment_percentage' => 30,
            'departament_id' => 1,
            'status_id' => 1
        ]);
    }
}
