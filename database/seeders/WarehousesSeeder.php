<?php

namespace Database\Seeders;

use App\Models\Warehouse;
use Illuminate\Database\Seeder;

class WarehousesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Warehouse::create([
            'name' => 'Local Principal Bogotá D.C.',
            'address' => 'Cl. 38b Sur #50c-7',
            'latitude' => '4.5975741',
            'length' => '-74.1299986',
            'city_id' => 1,
            'status_id' => 1,
            'principal' => 1
        ]);
    }
}
