<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'uid' => Hash::make('Nelson Eduardo Arevalo Cubides'),
            'names' => 'Nelson Eduardo',
            'last_names' => 'Arevalo Cubides',
            'email' => 'arneldesign@gmail.com',
            'avatar' => '0',
            'contraseña' => Hash::make('arneldesign@gmail.com'),
            'password' => Hash::make('password'),
            'document_type_id' => 1,
            'document' => '1018485712',
            'city_id' => 1,
            'account_type_id' => 1,
            'status_id' => 1,
            'quantity_orders' => 0
        ]);

        $user->assignRole('Super Admin');
    }
}
