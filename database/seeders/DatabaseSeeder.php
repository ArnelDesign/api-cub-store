<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            StatusesSeeder::class,
            CountriesSeeder::class,
            DepartamentsSeeder::class,
            CitiesSeeder::class,
            DocumentTypesSeeder::class,
            AccountTypesSeeder::class,
            RoleSeeder::class,
            UsersSeeder::class,
            WarehousesSeeder::class,
            UserWarehousesSeeder::class
        ]);
    }
}
